/*
+ SINGLE-RESPONSIBILITY PRINCIPLE:
=> This SuperWorker class only contains class member and methods directly related to it based on abstraction.
Created the robot class, seperate from the other worker classes. We implement IWorkable and overide the takeABreak method since robots don't eat.
Instead, the robot will "charge" during it's break. The robot starts fully charged.

*INTERFACE SEGREGATION PRINCIPLE: is applied when Robot class only implements IWorkable interface. 
=>Robot and Worker classes shares some common methods(behaviors), however we don't want to create Robot class by extending Worker class because Robot class doesn't use methods eat() and doesn't have name class member. So we created 2 different interface IFeedable and IWorkable. Then, We create Robot class implementing only IWorkable which is an interface only containing methods used both by Robot and Worker. At the same time, put all methods that Robot doesn't use in IFeedable interface 

*/

class Robot implements IWorkable {
  public int battery = 100;

  public String work() {  
      battery -= 30;
      return "I'm a working Robot!";
  }
  public void setCharge(int charge){
    battery += charge;
  }
  @Override
  public String takeABreak(){
    setCharge(30);
    return "Robot is taking a break and recharging!";
  }
}