/* 
+ SINGLE-RESPONSIBILITY PRINCIPLE:
=> The Worker class implements the SOLID principle single responsibility, which can be seen with the method getName(), for example, whose single responsibility is to return the name of the worker. 
*OPEN-CLOSE PRINCIPLE: 
This class also implements the Open-Closed principle, since it can easily be extended, which is exemplified in the SuperWorker class, but it is closed for modification since the existing code does not need to be modified for additional functionality.
*INTERFACE SEGREGATION PRINCIPLE: 
The interfaces IWorkable and IFeedable have both been implemented in this class since the worker eats and works. However, the method takeABreak() is overridden since the worker's break differs from that of the SuperWorker and Robot.


*/

class Worker implements IWorkable, IFeedable {
  String name = "";
  
	public String eat() 
	{
		if (name == "") 
    	{
       	return "This worker is eating already!";
    	}
    	else 
    	{
    		return name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
    	}
	}

  public String work() {
    if (name == "") {
      return "I'm working already.";
    }
    else {
      return name + " is working very hard!";
    }
  }

    @Override
    public String takeABreak() {
      String meal = eat();
      return "This worker is taking a break right now and " + meal;
    }

    public Boolean addpositive(int a, int b)
    {
      if ((a+b) > 0)
        return(true);
      return(false);
    }

    public String getName() 
  	{
    	return name;
  	}

  	public void setName(String name) 
  	{
      	this.name = name;
  	}
    
}