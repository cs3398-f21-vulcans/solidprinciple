/*
+ SINGLE-RESPONSIBILITY PRINCIPLE:
=> This SuperWorker class only contains class member and methods directly related to it based on abstraction.
A manager will manage a group of workers and robots. So we add a class member - called employee_list which uses ArrayList data type, and contains elements of classes implementing IWorkable interface.
_____________________________________________
+ OPEN-CLOSE PRINCIPLE: 
In the future, if there is any different type of Worker added to the list of employee, we don't need to modify the Manager class. We can create a class of the new type of worker by having this class implementing IWorkable interface or extends Worker class
*/


import java.util.ArrayList;

class Manager {

  ArrayList<IWorkable> employee_list = new ArrayList<IWorkable>();
  
	public void setWorker(IWorkable w) {
    employee_list.add(w);
	}

	public void manage() {
    System.out.println("Manager: Everyone starts to Work");
		for (int i = 0; i < employee_list.size(); i++) {
      System.out.println(employee_list.get(i).work());
    }
    System.out.println("-----------------------------------------");
  }

  public void giveABreak() {
    System.out.println("Manager: Everyone can take a break now");
		for (int i = 0; i < employee_list.size(); i++) {
      System.out.println(employee_list.get(i).takeABreak());
    }
    System.out.println("-----------------------------------------");
	}
  
}