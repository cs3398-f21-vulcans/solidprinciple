import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

/* 
- seperate main driver from interfaces and 
  create objects to call the reference to the workers
- a SOLID principle that can be applied to this would be the
  Open-Closed principle. 
- The worker objects that we fetch should be open 
  for extension but closed for modification. 
  This is the very definition of the Open-Closed principle.
  Also, each class has this program has one and only one reason to change, or only one job. 
  Thus, these classes were implemented using the Single-Responsibility Principle.
*/

public class Main
{   
  public static Manager manager1= new Manager();

  public static void main(String[] args) 
   {
      try 
      { 
        Robot robot1 = new Robot();
        SuperWorker super_worker1 = new SuperWorker();
        Worker worker1 = new Worker();
        //manager adds workers into the employee_list
        manager1.setWorker(robot1);
        manager1.setWorker(super_worker1);
        manager1.setWorker(worker1);
        //manager manages every workers to work
        manager1.manage();        
        // manager 
        manager1.giveABreak();
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.exit(0);

   }
 }






