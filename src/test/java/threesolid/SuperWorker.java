/*OPEN-CLOSE PRINCIPLE: is applied when SuperWorker is the Super Class extends Base Class Worker
=>  Because SuperWorker objects will behave differently in only work() and takeABreak() methods, and we don't want to modify those methods implementation in the base class- Worker. So we created this super class of Worker class, which is SuperWorker class and override those methods.

*SINGLE-RESPONSIBILITY PRINCIPLE:
=> This SuperWorker class only contains class member and methods directly related to it such as work(), takeABreak(), eat() ...

*INTERFACE SEGREGATION PRINCIPLE: is applied when Superworker was a super class of Worker class which handles methods in IWorkable interface and IFeedable interface.
*/
class SuperWorker extends Worker{

	public String work() {
    if (name == "") {
      return "I am a super worker";
    }
    else {
      return name + " is working super hard.";
    }
	}

  @Override
  public String takeABreak() {
    return "This super worker is taking a break in his personal room and "+ this.eat() ;
  }
}
