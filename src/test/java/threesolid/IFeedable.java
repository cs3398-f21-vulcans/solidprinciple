/*
+ INTERFACE SEGREGATION is applied when Robot class only implements IWorkable interface. 
=>Robot and Worker classes shares some common methods(behaviors), however we don't want to create Robot class by extending Worker class because Robot class doesn't use methods eat() and doesn't have name class member. So we created 2 different interface IFeedable and IWorkable. Then, We create Robot class implementing only IWorkable which is an interface only containing methods used both by Robot and Worker. At the same time, put all methods that Robot doesn't use in IFeedable interface 
*/
interface IFeedable {
		public String eat();
    public Boolean addpositive(int a, int b);
}   
